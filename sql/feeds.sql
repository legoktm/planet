CREATE TABLE feed (
	f_id int not null AUTO_INCREMENT,
	f_url varchar(1000) not null,
	f_updated timestamp not null,
	f_healthy bool not null,
	f_error blob,
	PRIMARY KEY (f_id),
	UNIQUE KEY (f_url)
)
CHARACTER SET 'utf8mb4';

CREATE TABLE post (
	p_id int not null AUTO_INCREMENT,
	p_feed int not null,
	p_tag varchar(200) not null,
	p_url varchar(1000) not null,
	p_title varchar(1000) not null,
	p_time timestamp not null,
	p_text blob not null,
	PRIMARY KEY (p_id),
	UNIQUE KEY (p_feed, p_tag)
)
CHARACTER SET 'utf8mb4';
