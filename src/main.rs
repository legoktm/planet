#[macro_use]
extern crate rocket;

use anyhow::{anyhow, Error, Result};
use chrono::NaiveDateTime;
use feed_rs::model::Entry;
use mime::Mime;
use mysql_async::Pool;
use rocket::fairing::AdHoc;
use rocket::fs::FileServer;
use rocket_dyn_templates::Template;
use serde::{Deserialize, Serialize};

mod crawl;
mod db;
mod web;

const CRAWL_CONCURRENCY: usize = 8;
const ITEMS_PER_FEED: usize = 20;
/// Poll feeds every hour
const POLL_FREQUENCY: u64 = 60 * 60;
const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Deserialize, Clone)]
struct Config {
    /// Name of this planet
    name: String,
    /// `mysql://` URI with database connection info
    database: String,
    /// List of feed URLs
    urls: Vec<String>,
}

#[derive(Debug, Serialize)]
struct Post {
    tag: String,
    url: String,
    title: String,
    time: NaiveDateTime, // UTC
    text: String,
}

impl TryFrom<Entry> for Post {
    type Error = Error;

    fn try_from(entry: Entry) -> std::result::Result<Self, Self::Error> {
        let item = Post {
            tag: entry.id,
            url: {
                if entry.links.is_empty() {
                    Err(anyhow!("no link provided"))?
                }
                entry.links[0].href.to_string()
            },
            // TODO: validate content_type
            // TODO: validate non-zero length
            title: entry.title.ok_or_else(|| anyhow!("missing title"))?.content,
            // first try published, then updated
            // todo: just use current date as fallback??
            time: match (entry.published, entry.updated) {
                (Some(published), _) => published.naive_utc(),
                (_, Some(updated)) => updated.naive_utc(),
                (None, None) => {
                    Err(anyhow!("neither published nor updated found"))?
                }
            },
            text: match (entry.content, entry.summary) {
                (Some(content), _) => {
                    check_text_mime(&content.content_type)?;
                    let body =
                        content.body.ok_or_else(|| anyhow!("missing body"))?;
                    sanitize(&body, &content.content_type)?
                }
                (_, Some(summary)) => {
                    check_text_mime(&summary.content_type)?;
                    let body = summary.content;
                    sanitize(&body, &summary.content_type)?
                }
                (None, None) => {
                    Err(anyhow!("neither content nor summary found"))?
                }
            },
        };
        Ok(item)
    }
}

fn check_text_mime(mime: &Mime) -> Result<()> {
    if mime.type_() != "text" {
        Err(anyhow!("non text/ mime used: {}", mime))
    } else {
        Ok(())
    }
}

fn sanitize(text: &str, mime: &Mime) -> Result<String> {
    let cleaned = match mime.to_string().as_str() {
        "text/plain" => ammonia::clean_text(text),
        "text/html" => ammonia::clean(text),
        mime => Err(anyhow!("unknown mime type: {}", mime))?,
    };
    Ok(cleaned)
}

#[launch]
fn rocket() -> _ {
    tracing_subscriber::fmt().init();
    let cfg: Config = toml::from_str(
        &std::fs::read_to_string("config.toml")
            .expect("failed to read config.toml"),
    )
    .expect("invalid config");
    let pool = Pool::new(cfg.database.as_str());
    rocket::build()
        .manage(cfg)
        .manage(pool)
        .mount("/", routes![web::index])
        .mount("/static", FileServer::from("static/"))
        .attach(Template::fairing())
        .attach(AdHoc::on_liftoff("crawler", |rocket| {
            // unwrap: Safe because we added Config + Pool state a few lines above
            let cfg = rocket.state::<Config>().unwrap().clone();
            let pool = rocket.state::<Pool>().unwrap().clone();
            Box::pin(async move {
                let _ = crawl::start_crawl(&cfg, &pool).await;
            })
        }))
}
