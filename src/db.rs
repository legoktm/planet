use crate::{Config, Post};
use anyhow::{Context, Result};
use chrono::NaiveDateTime;
use mysql_async::prelude::Queryable;
use mysql_async::Conn;
use std::collections::HashMap;
use tracing::debug;

pub(crate) struct FeedRow {
    pub(crate) id: usize,
    pub(crate) url: String,
    pub(crate) updated: NaiveDateTime, // UTC
    pub(crate) healthy: bool,
    pub(crate) error: Option<String>,
}

pub(crate) struct PostRow {
    pub(crate) id: usize,
    pub(crate) feed: usize,
    pub(crate) item: Post,
}

/// Get the list of feeds from the database
pub(crate) async fn load_feeds(conn: &mut Conn) -> Result<Vec<FeedRow>> {
    let results = conn
        .query_map(
            r#"
SELECT f_id, f_url, f_updated, f_healthy, f_error FROM feed
"#,
            |(f_id, f_url, f_updated, f_healthy, f_error): (
                usize,
                String,
                NaiveDateTime,
                bool,
                Option<Vec<u8>>,
            )| {
                FeedRow {
                    id: f_id,
                    url: f_url,
                    updated: f_updated,
                    healthy: f_healthy,
                    error: f_error.map(|bytes| {
                        String::from_utf8(bytes).unwrap_or_else(|err| {
                            panic!(
                                "invalid utf-8 in f_error, row {f_id}: {err}"
                            )
                        })
                    }),
                }
            },
        )
        .await?;
    Ok(results)
}

/// Synchronize the `feed` table with configuration
pub(crate) async fn update_feeds(conn: &mut Conn, cfg: &Config) -> Result<()> {
    let mut existing: HashMap<String, usize> = {
        let results: Vec<(String, usize)> =
            conn.query("SELECT f_url, f_id FROM feed").await?;
        results.into_iter().collect()
    };
    let mut insert = vec![];
    for url in &cfg.urls {
        if existing.contains_key(url) {
            existing.remove(url);
        } else {
            insert.push(url.to_string());
        }
    }
    // Anything left in `in_db` is not in configuration, so delete it
    for id in existing.into_values() {
        conn.exec_drop("DELETE FROM feed WHERE id=?", (id,)).await?;
    }
    // Insert the new values, initialize to a "random" datetime
    // that is older than the poll frequency so it is processed right away
    for url in insert {
        conn.exec_drop(
            r#"
INSERT INTO feed (f_url, f_updated, f_healthy)
VALUES (?, "2001-01-15 00:00:00", true)
"#,
            (url,),
        )
        .await?;
    }
    Ok(())
}

/// Load the most recent 100 posts
pub(crate) async fn load_posts(conn: &mut Conn) -> Result<Vec<PostRow>> {
    let results = conn
        .query_map(
            r#"
SELECT p_id, p_feed, p_tag, p_url, p_title, p_time, p_text FROM post
ORDER BY p_time DESC
LIMIT 100
"#,
            |(p_id, p_feed, p_tag, p_url, p_title, p_time, p_text): (
                usize,
                usize,
                String,
                String,
                String,
                NaiveDateTime,
                Vec<u8>,
            )| {
                PostRow {
                    id: p_id,
                    feed: p_feed,
                    item: Post {
                        tag: p_tag,
                        url: p_url,
                        title: p_title,
                        time: p_time,
                        text: String::from_utf8(p_text)
                            .context(format!("row {p_id}"))
                            .unwrap(),
                    },
                }
            },
        )
        .await?;
    Ok(results)
}

pub(crate) async fn update_posts(
    conn: &mut Conn,
    posts: Vec<Post>,
    feed: &FeedRow,
) -> Result<()> {
    debug!("inserting {} posts into db", posts.len());
    for post in posts {
        conn.exec_drop(
            r#"
INSERT IGNORE INTO post (p_feed, p_tag, p_url, p_title, p_time, p_text)
VALUES (?, ?, ?, ?, ?, ?)
"#,
            (
                feed.id, post.tag, post.url, post.title, post.time, post.text,
            ),
        )
        .await?;
    }
    Ok(())
}
