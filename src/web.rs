use crate::{db, Config, Post};
use anyhow::Result;
use mysql_async::Pool;
use rocket::http::Status;
use rocket::State;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct IndexTemplate {
    name: String,
    posts: Vec<Post>,
}

#[derive(Serialize)]
struct ErrorTemplate {
    error: String,
}

#[get("/")]
pub(crate) async fn index(
    pool: &State<Pool>,
    config: &State<Config>,
) -> Result<Template, (Status, Template)> {
    match build_index(pool, config).await {
        Ok(temp) => Ok(Template::render("index", temp)),
        Err(err) => Err((
            Status::InternalServerError,
            Template::render(
                "error",
                ErrorTemplate {
                    error: err.to_string(),
                },
            ),
        )),
    }
}

async fn build_index(pool: &Pool, config: &Config) -> Result<IndexTemplate> {
    let mut conn = pool.get_conn().await?;
    let posts = db::load_posts(&mut conn)
        .await?
        .into_iter()
        .map(|row| row.item)
        .collect();
    Ok(IndexTemplate {
        name: config.name.to_string(),
        posts,
    })
}
