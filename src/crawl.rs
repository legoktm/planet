use crate::db::FeedRow;
use crate::{
    db, Config, Post, CRAWL_CONCURRENCY, ITEMS_PER_FEED, POLL_FREQUENCY,
    VERSION,
};
use anyhow::Result;
use mysql_async::Pool;
use reqwest::Client;
use std::sync::Arc;
use tokio::sync::Semaphore;
use tokio::time;
use tracing::{debug, error, info};

pub(crate) async fn start_crawl(cfg: &Config, pool: &Pool) -> Result<()> {
    let mut conn = pool.get_conn().await?;
    db::update_feeds(&mut conn, cfg).await?;
    let client = Client::builder()
        .user_agent(format!("{} trappist-1/{VERSION}", cfg.name))
        .build()?;

    let feeds = db::load_feeds(&mut conn).await?;
    drop(conn);
    let semaphore = Arc::new(Semaphore::new(CRAWL_CONCURRENCY));
    for feed in feeds {
        start_crawl_feed(feed, &client, pool, semaphore.clone());
    }

    Ok(())
}

fn start_crawl_feed(
    feed: FeedRow,
    client: &Client,
    pool: &Pool,
    semaphore: Arc<Semaphore>,
) {
    let client = client.clone();
    let pool = pool.clone();
    tokio::spawn(async move {
        loop {
            // TODO: we should only crawl if it's been 60min since the last crawl
            match crawl_feed(&feed, &client, &pool, &semaphore).await {
                Ok(()) => {
                    info!("successfully crawled #{}", feed.id);
                }
                Err(err) => {
                    error!("crawling #{} failed: {err}", feed.id);
                }
            };
            // TODO: update `feed` table here
            time::sleep(time::Duration::from_secs(POLL_FREQUENCY)).await;
        }
    });
}

async fn crawl_feed(
    feed: &FeedRow,
    client: &Client,
    pool: &Pool,
    semaphore: &Semaphore,
) -> Result<()> {
    let permit = semaphore.acquire().await.unwrap();
    info!("fetching {}", &feed.url);
    let resp = client.get(&feed.url).send().await?.error_for_status()?;
    drop(permit);
    let raw = resp.text().await?;
    debug!("parsing result from {}", &feed.url);
    let items = parse(raw)?;
    debug!("got {} items from {}", items.len(), &feed.url);
    let mut conn = pool.get_conn().await?;
    db::update_posts(&mut conn, items, feed).await?;
    Ok(())
}

fn parse(raw: String) -> Result<Vec<Post>> {
    let feed = feed_rs::parser::parse(raw.as_bytes())?;
    let mut items = vec![];
    for entry in feed.entries {
        let item = match Post::try_from(entry.clone()) {
            Ok(item) => item,
            Err(err) => {
                error!("[error] converting {} failed: {}", entry.id, err);
                continue;
            }
        };
        items.push(item);
        // TODO: we should drop very old posts here
        if items.len() >= ITEMS_PER_FEED {
            break;
        }
    }

    Ok(items)
}
